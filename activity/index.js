/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	function information() {
	let fullName = prompt("Enter your full name: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your location: ");
	alert("Thank you for the input!");
	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location);
	}

	information();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function topFiveBands() {
		let firstBand = "My Chemical Romance";
		let secondBand = "Panic! At The Disco";
		let thirdBand = "Three Days Grace";
		let fourthBand = "Queen";
		let fifthBand = "Black Veil Brides";

		console.log("1. " + firstBand);
		console.log("2. " + secondBand);
		console.log("3. " + thirdBand);
		console.log("4. " + fourthBand);
		console.log("5. " + fifthBand);
	};

	topFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topFiveMovies() {
		let firstMovie = "The Mask";
		let secondMovie = "Bruce Almighty";
		let thirdMovie = "Ace Ventura: Pet Detective";
		let fourthMovie = "Ace Ventura: When Nature Calls";
		let fifthMovie = "Me, Myself & Irene";

		console.log("1. " + firstMovie);
		console.log("Rotten Tomatoes Rating: 80%")
		console.log("2. " + secondMovie);
		console.log("Rotten Tomatoes Rating: 48%")
		console.log("3. " + thirdMovie);
		console.log("Rotten Tomatoes Rating: 48%")
		console.log("4. " + fourthMovie);
		console.log("Rotten Tomatoes Rating: 21%")
		console.log("5. " + fifthMovie);
		console.log("Rotten Tomatoes Rating: 48%")
	}

	topFiveMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

/*console.log(friend1);
console.log(friend2);*/

printFriends();